package biom4st3r.asm.reflection;

import java.lang.reflect.Method;

public interface MethodRef<R> {
    
    @SuppressWarnings("unchecked")
    default R invoke(Object host,Object... obj) {
        try {
            return (R) getMethod().invoke(host, obj);
        } catch (Throwable t) {
            t.printStackTrace();
            return null;
        }
    }

    Method getMethod() throws Throwable;

    static <R> MethodRef<R> getMethod(Class<?> target, String name, Class<?>... args) {
        try {
            Method method = target.getDeclaredMethod(name, args);
            method.setAccessible(true);
            return () -> method;
        }
        catch(Throwable t) {
            throw new RuntimeException(t);
        }
    }

    static <R> MethodRef<R> getMethod(String className, String name, Class<?>... args) {
        try {
            Method method = Class.forName(className).getDeclaredMethod(name, args);
            method.setAccessible(true);
            return () -> method;
        }
        catch(Throwable t) {
            throw new RuntimeException(t);
        }
    }

    static <R> MethodRef<R> getMethod(Class<?> target, int index) {
        try {
            Method method = target.getDeclaredMethods()[index];
            method.setAccessible(true);
            return () -> method;
        }
        catch(Throwable t) {
            throw new RuntimeException(t);
        }
    }

    static <R> MethodRef<R> getMethod(String className, int index) {
        try {
            Method method = Class.forName(className).getDeclaredMethods()[index];
            method.setAccessible(true);
            return () -> method;
        }
        catch(Throwable t) {
            throw new RuntimeException(t);
        }
    }
}
