package biom4st3r.asm.reflection;

import java.lang.reflect.Constructor;

public interface CtorRef<R>
{
    @SuppressWarnings("unchecked")
    default R newInstance(Object... objs)
    {
        try
        {
           return (R)_ctor().newInstance(objs);
        }
        catch(Throwable t)
        {
            throw new RuntimeException(t);
        }
    }

    Constructor<?> _ctor() throws Throwable;

    static <R> CtorRef<R> getCtor(Class<?> target,int index,Class<?>... args)
    {
        try
        {
            if(args != null)
            {
                Constructor<?> ctor = target.getDeclaredConstructor(args);
                ctor.setAccessible(true);
                return ()->ctor;
            }
            else
            {
                Constructor<?> ctor = target.getDeclaredConstructors()[index];
                ctor.setAccessible(true);
                return ()->ctor;
            }
        }
        catch(Throwable t)
        {
            throw new RuntimeException(t);
        }
    }
}
