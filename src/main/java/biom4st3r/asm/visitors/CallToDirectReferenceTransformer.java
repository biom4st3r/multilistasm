package biom4st3r.asm.visitors;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;


public class CallToDirectReferenceTransformer extends ClassVisitor {

    public CallToDirectReferenceTransformer(int api, ClassVisitor classVisitor) {
        super(api, classVisitor);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
            String[] exceptions) {
        MethodVisitor mn =  super.visitMethod(access, name, descriptor, signature, exceptions);
        // new $MethodNode().accept(mn);
        return mn;
    }
    


    // private static class $MethodNode extends MethodNode {
    //     public void asdf() {
    //         AbstractInsnNode node = this.instructions.getFirst();
    //         while(node.getNext() != null) {
    //             node = node.getNext();
    //             if(node instanceof MethodInsnNode) {
    //                 MethodInsnNode min = (MethodInsnNode) node;
    //                 if(min.name.startsWith("_get")) {
    //                     // get the array index var loading insn
    //                     AbstractInsnNode arrayIndex = min.getPrevious().getPrevious(); 
    //                     // convert arrayindex into real number
    //                     int i = 0; 
    //                     // replace inde xvar laoding insn with field get for the array
    //                     this.instructions.set(arrayIndex, new FieldInsnNode(Opcodes.GETFIELD, min.owner, i+"array", min.desc.split(")")[1]));
    //                     // replace method invokation with Array XALOAD
    //                     this.instructions.set(min, new InsnNode(Opcodes.LALOAD));
    //                     node.accept(methodVisitor);
    //                 }
    //             }
    //         }
    //     }
    // }
    
}
