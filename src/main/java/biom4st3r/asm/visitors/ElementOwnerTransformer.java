package biom4st3r.asm.visitors;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.commons.InstructionAdapter;

public class ElementOwnerTransformer extends InstructionAdapter {

    private String prevOwner;
    private String newOwner;
    public ElementOwnerTransformer(int api, MethodVisitor methodVisitor,String prevOwner, String newOwnerInteralName) {
        super(api, methodVisitor);
        this.prevOwner = prevOwner;
        this.newOwner = newOwnerInteralName;
    }

    @Override
    public void invokeinterface(String owner, String name, String descriptor) {
        super.invokeinterface(handleOwner(owner), name, descriptor);
    }
    @Override
    public void invokestatic(String owner, String name, String descriptor, boolean isInterface) {
        super.invokestatic(handleOwner(owner), name, descriptor, isInterface);
    }
    @Override
    public void invokevirtual(String owner, String name, String descriptor, boolean isInterface) {
        super.invokevirtual(handleOwner(owner), name, descriptor, isInterface);
    }
    @Override
    public void getstatic(String owner, String name, String descriptor) {
        super.getstatic(handleOwner(owner), name, descriptor);
    }
    @Override
    public void putstatic(String owner, String name, String descriptor) {
        super.putstatic(handleOwner(owner), name, descriptor);
    }
    private String handleOwner(String owner) {
        return owner.equals(prevOwner) ? newOwner : owner;
    }

    @Override
    public void getfield(String owner, String name, String descriptor) {
        super.getfield(handleOwner(owner), name, descriptor);
    }

    @Override
    public void putfield(String owner, String name, String descriptor) {
        super.putfield(handleOwner(owner), name, descriptor);
    }
    
}
