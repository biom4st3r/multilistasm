// package biom4st3r.asm;

// import java.lang.reflect.Array;
// import java.util.Arrays;
// import java.util.concurrent.locks.ReentrantLock;
// import java.util.function.Consumer;


// /**
//  * A Object for maintaining multiple resizable arrays of different types.<p>.
//  * T represents an interaction medium for the types held in the Multilist.
//  */
// public abstract class MultiList<T> {

//     public static final int DEFAULT_SIZE = 16;

//     /**
//      * Because this can be accessed by the MAIN thread and NETTIO thread it must be
//      * locked during iteration and reinitilization
//      */
//     protected final ReentrantLock LOCK = new ReentrantLock();

//     public int getSignature() {
//         return this.index + this.getCollection(0).length + this.getCollection(0).hashCode();
//     }

//     protected Object[] collections;
    
//     protected int index = 0;

//     /**
//      * Will return an array of the types of collections this will hold.<p>
//      * @return
//      */
//     protected abstract Class<?>[] getTypes();

//     /**
//      * must be borrowed during iteration
//      * @param consumer
//      */
//     public abstract void forEach(Consumer<T> consumer);

//     @SuppressWarnings({"unchecked", "deprecation"})
//     public <R extends MultiList<T>> R getCopy() {
//         try {
//             R r = (R) this.getClass().newInstance();
//             r.collections = Arrays.copyOf(this.collections, this.length());
//             r.index = index;
//             r.clone = this.clone+1;
//             return r;
//         } catch (InstantiationException | IllegalAccessException e) {
//             throw new RuntimeException(e);
//         }
        
//     }
//     // public abstract CompoundTag toTag(CompoundTag tag);
//     // public abstract void fromTag(CompoundTag tag);

//     /**
//      * Returns a T with the values held at index i
//      * @param <R>
//      * @param i
//      * @return
//      */
//     public abstract T get(int i);

//     /**
//      * passes T to the consumer, then reassigns the indices to the altered values of T
//      * @param index
//      * @param entry
//      */
//     public abstract void modifyEntry(int index, Consumer<T> entry);

//     @SuppressWarnings({"unchecked"})
//     protected Object getCollection(int index) {
//         return this.collections[index];
//     }

//     public MultiList(int defaultSize) {
//         this.reinit(defaultSize);
//     }

//     public MultiList() {
//         this.reinit(DEFAULT_SIZE);
//     }
//     protected MultiList(int defaultSize, int loadFactor) {
//         this.reinit(loadFactor);
//         this.loadFactor = loadFactor;
//     }
    
//     public int loadFactor = DEFAULT_SIZE;

//     public boolean isEmpty() {
//         return this.length() == 0;
//     }

//     /**
//      * removed null entries and shrinks contained arrays.
//      * Check back to front until it finds a not null value and resizes down to that point
//      */
//     public void trim() {
//         if(this.getCollection(0).length > this.index) {
//             this.resize(this.index - this.getCollection(0).length);
//         }
//     }

//     /**
//      * Reinits arrays to default
//      */
//     public void clear() {
//         this.reinit(this.loadFactor);
//     }

//     protected final void borrow(Runnable r) {
//         this.LOCK.lock();
//         r.run();
//         this.LOCK.unlock();
//     }

//     /**
//      * Reassigns contained arrays to {@code size} and resets index
//      * 
//      * @param size
//      */
//     public void reinit(int size) {
//         this.borrow(()-> {
//             collections = new Object[this.getTypes().length];
//             int i = 0;
//             for(Class<?> type : this.getTypes()) {
//                 collections[i] = Array.newInstance(type, size);
//                 i++;
//             }
//             this.index = 0;
//         });
//     }

//     /**
//      * For internal calling only. intended that you mutate collections inside of Runnable r<p>
//      * Any MultiList.add(*) created, but either use this or call checkSize,borrow, and index++ like this.
//      * @param r
//      */
//     protected final void add(Runnable r) {
//         this.checkSize();
//         borrow(r);
//         this.index++;
//     }

//     /**
//      * checks if the array is at max capacity and expands if needed
//      */
//     protected final void checkSize() {
//         if (this.index == this.getCollection(0).length) {
//             this.resize(this.loadFactor);
//         }
//     }

//     public final T getLatest()
//     {
//         return this.get(index-1);
//     }

//     public void revokeLatest()
//     {
//         this.index -= 1;
//     }

//     protected void resize(int expansion) {
//         int newSize = this.getCollection(0).length + expansion;
//         // locker(()-> { COW?
//         // this.names = Arrays.copyOf(names, newSize);
//         // this.dims = Arrays.copyOf(dims, newSize);
//         // this.poses = Arrays.copyOf(poses, newSize);
//         // this.isPublic = Arrays.copyOf(isPublic, newSize);
//         for(int i = 0; i < this.getTypes().length; i ++) {
//             this.collections[i] = Arrays.copyOf(this.getCollection(i), newSize);
//         }
//         // });
//     }


//     int clone = 0;


//     /** CONST FOR SERIALIZATION */
//     public static final String
//         NAMES = "names",
//         LENGTH = "length",
//         POSITIONS = "poses",
//         DIMENSIONS = "dims",
//         PUBLICS = "pubs";


//     public void remove(int index) {
//         this.borrow(()-> {
//             for(int i = 0; i < this.getTypes().length; i++) {
//                 if(i == this.length()) {
//                     this.revokeLatest();
//                     continue;
//                 }
//                 System.arraycopy(this.getCollection(i),    index+1, this.getCollection(i),    index, this.index-(index+1));
//             }
//         });
//         this.revokeLatest();
//         this.trim();
//     }

//     public int length() {
//         return this.index;
//     }

// }
