package biom4st3r.asm;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;

// import org.openjdk.jmh.annotations.Benchmark;
// import org.openjdk.jmh.annotations.BenchmarkMode;
// import org.openjdk.jmh.annotations.Mode;

// @BenchmarkMode(Mode.All)
public class Main {

    public static void main(String[] args) {

        // MyMultiList asm = MultiListAsm.generate(MyMultiList.class, 12, Stopwatch.class, long.class, Map.class);
        // test(asm);
        // for(int i = 0; i < asm.getLength(); i++) {
        //     MyMultiList asm2 = asm.getCopy();
        //     Preconditions.checkState(asm.get(i).l == asm2.get(i).l);
        // }
        // try {
        //     org.openjdk.jmh.Main.main(args);
        // } catch (IOException e) {
        //     // TODO Auto-generated catch block
        //     e.printStackTrace();
        // }
        Exception e = new RuntimeException();
        System.out.println("hello");
        for(StackTraceElement i : e.getStackTrace()) {
            System.out.println(i.toString());
        }
    }

    // private static void test(MyMultiList asm) {
    //     Preconditions.checkState(asm.getArraySize() == 12, "Array grow wrong0. should be %s is %s", 12, asm.getArraySize());
    //     asm.add(Stopwatch.createUnstarted(), 0, Maps.newHashMap());
    //     asm.add(Stopwatch.createUnstarted(), 1, Maps.newHashMap());
    //     asm.add(Stopwatch.createUnstarted(), 2, null);
    //     asm.add(null, 3, Maps.newHashMap());
    //     asm.add(Stopwatch.createUnstarted(), 4, Maps.newHashMap());
    //     asm.add(Stopwatch.createUnstarted(), 4, Maps.newHashMap());
    //     asm.add(Stopwatch.createUnstarted(), 4, Maps.newHashMap());
    //     asm.add(Stopwatch.createUnstarted(), 4, Maps.newHashMap());
    //     asm.add(Stopwatch.createUnstarted(), 4, Maps.newHashMap());
    //     asm.add(Stopwatch.createUnstarted(), 4, Maps.newHashMap());
    //     asm.add(Stopwatch.createUnstarted(), 4, Maps.newHashMap());
    //     asm.add(Stopwatch.createUnstarted(), 4, Maps.newHashMap());
    //     Preconditions.checkState(asm.getArraySize() == 12, "Array grow wrong1. should be %s is %s", 12, asm.getArraySize());
    //     asm.add(Stopwatch.createUnstarted(), 4, Maps.newHashMap());
    //     Preconditions.checkState(asm.getArraySize() == 24, "Array grow wrong2. should be %s is %s", 24, asm.getArraySize());
    //     Preconditions.checkState(asm.get(1).map != null, "Index 1 map shoudn't be null");
    //     Preconditions.checkState(asm.get(2).map == null, "Index 2 map shoud be null");
    //     Preconditions.checkState(asm.get(2).l == 2, "Index 2 long shoud be 2");
    //     Preconditions.checkState(asm.get(3).sw == null, "Index 3 sw shoud be null");
    //     asm.modify(7, (instance)-> {
    //         instance.l = 42L;
    //         instance.sw = null;
    //         ((Map)instance.map).put(5, 11);
    //     });
    //     Preconditions.checkState(asm.get(7).l == 42L, "Modifiy didn't work on long");
    //     Preconditions.checkState(asm.get(7).sw == null, "Modifiy didn't work on sw");
    //     Preconditions.checkState(((Map<Integer,Integer>)asm.get(7).map).get(5) == 11, "Modifiy didn't work on map");
    //     asm.remove(3);
    //     Preconditions.checkState(asm.get(3).l == 4, "move didn't work properly");
        
    //     // asm.set(100, null, 1, null);
    // }

    public static MyMultiList mmlist = MultiListAsm.generate(MyMultiList.class, 16, Stopwatch.class, long.class, Map.class);
    public static List<Stopwatch> l0 = Lists.newArrayList();
    public static List<Long> l1 = Lists.newArrayList();
    public static List<Map<?,?>> l2 = Lists.newArrayList();
    
    // @Benchmark
    public static void bench0() {

        // for(int i = 0; i < 36; i++) {
            
        //     mmlist.add(Stopwatch.createStarted(), i, new HashMap<>());
        // }
        // for(int i = 0; i < 36 / 2; i++) {
            
        //     mmlist.remove(0);
        //     mmlist.remove(mmlist.getLength()-1);
        // }
        mmlist.test0();
    }
    
    // @Benchmark
    public static void bench1() {
        // for(int i = 0; i < 36; i++) {
        //     l0.add(Stopwatch.createStarted());
        //     l1.add((long) i);
        //     l2.add(new HashMap<>());
        // }
        // for(int i = 0; i < 36 / 2; i++) {
        //     l0.remove(0);
        //     l0.remove(l0.size()-1);
        //     l1.remove(0);
        //     l1.remove(l1.size()-1);
        //     l2.remove(0);
        //     l2.remove(l2.size()-1);
        // }
        mmlist.test1();
    }

}
