package biom4st3r.asm;

import java.util.Random;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class OpcodeMethodVisitor extends MethodVisitor {

    public OpcodeMethodVisitor(int api, MethodVisitor methodVisitor) {
        super(api, methodVisitor);
    }
    public OpcodeMethodVisitor(int api) {
        super(api);
    }

    Random random = new Random();

    @Override
    public void visitVarInsn(int opcode, int var) {
        // if(random.nextInt(3) == 0) this.NOP();
        super.visitVarInsn(opcode, var);
    }

    @Override
    public void visitInsn(int opcode) {
        // if(random.nextInt(3) == 0) this.NOP();
        super.visitInsn(opcode);
    }

    public void ASTORE(int index) {
        this.visitVarInsn(Opcodes.ASTORE, index);
    }
    public void ISTORE(int index) {
        this.visitVarInsn(Opcodes.ISTORE, index);
    }
    public void DSTORE(int index) {
        this.visitVarInsn(Opcodes.DSTORE, index);
    }
    public void FSTORE(int index) {
        this.visitVarInsn(Opcodes.FSTORE, index);
    }
    public void LSTORE(int index) {
        this.visitVarInsn(Opcodes.LSTORE, index);
    }


    public void AASTORE(int index) {
        this.visitVarInsn(Opcodes.AASTORE, index);
    }
    public void BASTORE(int index) {
        this.visitVarInsn(Opcodes.BASTORE, index);
    }
    public void CASTORE(int index) {
        this.visitVarInsn(Opcodes.CASTORE, index);
    }
    public void IASTORE(int index) {
        this.visitVarInsn(Opcodes.IASTORE, index);
    }
    public void LASTORE(int index) {
        this.visitVarInsn(Opcodes.LASTORE, index);
    }
    public void FASTORE(int index) {
        this.visitVarInsn(Opcodes.FASTORE, index);
    }
    public void DASTORE(int index) {
        this.visitVarInsn(Opcodes.DASTORE, index);
    }

    public void DUP() {
        this.visitInsn(Opcodes.DUP);
    }

    public void ALOAD(int index) {
        this.visitVarInsn(Opcodes.ALOAD, index);
    }
    public void ILOAD(int index) {
        this.visitVarInsn(Opcodes.ILOAD, index);
    }
    public void DLOAD(int index) {
        this.visitVarInsn(Opcodes.DLOAD, index);
    }
    public void FLOAD(int index) {
        this.visitVarInsn(Opcodes.FLOAD, index);
    }
    public void LLOAD(int index) {
        this.visitVarInsn(Opcodes.LLOAD, index);
    }

    public void AALOAD(int index) {
        this.visitVarInsn(Opcodes.AALOAD, index);
    }
    public void BALOAD(int index) {
        this.visitVarInsn(Opcodes.BALOAD, index);
    }
    public void CALOAD(int index) {
        this.visitVarInsn(Opcodes.CALOAD, index);
    }
    public void IALOAD(int index) {
        this.visitVarInsn(Opcodes.IALOAD, index);
    }
    public void LALOAD(int index) {
        this.visitVarInsn(Opcodes.LALOAD, index);
    }
    public void FALOAD(int index) {
        this.visitVarInsn(Opcodes.FALOAD, index);
    }
    public void DALOAD(int index) {
        this.visitVarInsn(Opcodes.DALOAD, index);
    }

    public void GET_FIELD(String owner, String fieldName, String desc) {
        this.visitFieldInsn(Opcodes.GETFIELD, owner, fieldName, desc);
    }
    public void GET_FIELD(String owner, String fieldName, Class<?> desc) {
        this.GET_FIELD(owner, fieldName, org.objectweb.asm.Type.getDescriptor(desc));
    }

    public void PUT_FIELD(String owner, String fieldName, String desc) {
        this.visitFieldInsn(Opcodes.PUTFIELD, owner, fieldName, desc);
    }
    public void PUT_FIELD(String owner, String fieldName, Class<?> desc) {
        this.PUT_FIELD(owner, fieldName, org.objectweb.asm.Type.getDescriptor(desc));
    }

    public void RETURN() {
        this.visitInsn(Opcodes.RETURN);
    }
    public void IRETURN() {
        this.visitInsn(Opcodes.IRETURN);
    }
    public void DRETURN() {
        this.visitInsn(Opcodes.DRETURN);
    }
    public void FRETURN() {
        this.visitInsn(Opcodes.FRETURN);
    }
    public void LRETURN() {
        this.visitInsn(Opcodes.LRETURN);
    }
    public void ARETURN() {
        this.visitInsn(Opcodes.ARETURN);
    }

    public void ARRAY_LENGTH() {
        this.visitInsn(Opcodes.ARRAYLENGTH);
    }

    public void NEW_ARRAY(int arrayType) {
        this.visitIntInsn(Opcodes.NEWARRAY, arrayType);
    }

    public void ANEW_ARRAY(String interalName) {
        this.visitTypeInsn(Opcodes.ANEWARRAY, interalName);
    }
    public void CHECKCAST(String interalName) {
        this.visitTypeInsn(Opcodes.CHECKCAST, interalName);
    }
    public void INSTANCEOF(String internalName) {
        this.visitTypeInsn(Opcodes.INSTANCEOF, internalName);
    }
    public void NEW(String internalName) {
        this.visitTypeInsn(Opcodes.NEW, internalName);
    }

    public void NOP() {
        this.visitInsn(Opcodes.NOP);
    }

    public void IADD() {
        this.visitInsn(Opcodes.IADD);
    }
    public void ISUB() {
        this.visitInsn(Opcodes.ISUB);
    }
    public void IMUL() {
        this.visitInsn(Opcodes.IMUL);
    }
    public void IDIV() {
        this.visitInsn(Opcodes.IDIV);
    }
}
