package biom4st3r.asm;

import java.util.Map;
import java.util.function.Consumer;

import com.google.common.base.Stopwatch;


public class MyMultiList extends MultiListAsm {

    public static class Instance {
        public Stopwatch sw;
        public long l;
        public Map<?,?> map;
        public Instance(Stopwatch sw, long l, Map<?,?> map) {
            this.reassign(sw,l,map);
        }
        public Instance() {
        }
        public void reassign(Stopwatch sw2, long l2, Map<?, ?> map2) {
            this.sw = sw2;
            this.l = l2;
            this.map = map2;
        }
    }

    public MyMultiList(int loadFactor) {
        super(loadFactor);
    }

    public void add(Stopwatch sw, long l, Map<?,?> map) {
        this.set(this.index, sw, l, map);
        this.index++;
    }

    public MyMultiList.Instance get(int index) {
        return new Instance(this._getObject(0, index), this._getLong(1, index), this._getObject(2, index));
    }
    public void set(int index, Stopwatch sw, long l, Map<?,?> map) {
        this.lock();
        this._putObject(0, index, sw);
        this._putLong(1, index, l);
        this._putObject(2, index, map);
        this.unlock();
    }
    public void modify(int index, Consumer<MyMultiList.Instance> consumer) {
        MyMultiList.Instance i = this.get(index);
        this.lock();
        consumer.accept(i);
        this.unlock();
        this.set(index, i);
    }
    public void set(int index, MyMultiList.Instance i) {
        this.set(index, i.sw, i.l, i.map);
    }
    public void forEach(Consumer<MyMultiList.Instance> consumer) {
        MyMultiList.Instance instance = new Instance();
        this.lock();
        for(int i = 0; i < this.getLength(); i++) {
            instance.reassign(this._getObject(0, index), this._getLong(1, index), this._getObject(2, index));
            consumer.accept(instance);
        }
        this.unlock();
    }
    
    
}