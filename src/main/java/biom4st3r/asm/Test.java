package biom4st3r.asm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import org.objectweb.asm.tree.ClassNode;

public class Test {
    public static List<ClassNode> init(Object object, boolean z, int i, long j, Stopwatch sw) {
        List<ClassNode> list = Lists.newArrayList();
        Iterator<ClassNode> iter = new ArrayList().iterator();

        while(iter.hasNext()) {
            ClassNode cv = iter.next();
            if(z || cv.name.contains("fff")) {
                System.out.println();
                list.add(new ClassNode());
            }
        }
        for(int k = 0; k < list.size(); k++) {
            if(list.get(k).access == 44 || list.get(k).fields.size() == 21) {
                System.out.println();
            }
        }
        return list;
    }
}
