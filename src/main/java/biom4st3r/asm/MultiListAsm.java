package biom4st3r.asm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Resources;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.util.CheckClassAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import biom4st3r.asm.Asm.Primitive;
import biom4st3r.asm.Asm.Stack;
import biom4st3r.asm.reflection.CtorRef;
import biom4st3r.asm.reflection.MethodRef;
import biom4st3r.asm.visitors.ElementOwnerTransformer;

public class MultiListAsm {

    public static void print(long o) {
        System.out.println(o);
    }

    public static class MyClassVisitor extends ClassVisitor {
        public MyClassVisitor(ClassVisitor classVisitor) {
            super(Opcodes.ASM8, classVisitor);
        }

        @Override
        public void visitInnerClass(String name, String outerName, String innerName, int access) {
            // super.visitInnerClass(name, outerName, innerName, access);
        }
        

        @Override
        public OpcodeMethodVisitor visitMethod(int access, String name, String descriptor, String signature,
                String[] exceptions) {
            
            return new OpcodeMethodVisitor(api, super.visitMethod(access, name, descriptor, signature, exceptions));
        }


    }

    public static final File currentDirFile = new File(System.getProperty("user.dir"));

    public static final MethodRef<Class<?>> defineClass = MethodRef.getMethod(ClassLoader.class, "defineClass", byte[].class,int.class,int.class); 
    
    static Class<?> arrayType(Class<?> clazz) {
        return Array.newInstance(clazz, 0).getClass();
    }
    
    @SuppressWarnings({"unchecked"})
    public static <T extends MultiListAsm> T generate(Class<T> parent, int loadFactor, Class<?>... clazzes) {
        System.out.println(clazzes[0].getName());
        System.out.println(HashSet.class.getName());
        Preconditions.checkState(MultiListAsm.class.isAssignableFrom(parent)); 
        Map<String,Class<?>> map = Maps.newHashMap();
        StringBuilder builder = new StringBuilder("AsmMultiList");
        for(Class<?> c : clazzes) {
            builder.append('$');
            builder.append(c.getName().replace('.', '_'));
        }
        String clazzPath = "biom4st3r.asm.";
        String clazzName = builder.toString();

        if(map.containsKey(clazzName)) {
            CtorRef<MultiListAsm> ref = CtorRef.getCtor(map.get(clazzName), 0, int.class);
            return (T) ref.newInstance(loadFactor);
        }
        String certifiedClazzName = clazzPath.replace('.', '/') + clazzName;

        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);

        try {
            ClassVisitor c = new ClassVisitor(Opcodes.ASM8,cw){

                @Override
                public void visitInnerClass(String name, String outerName, String innerName, int access) {
                    
                }

                @Override
                public void visitOuterClass(String owner, String name, String descriptor) {
                    
                }

                public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
                    if(name.equals("<init>")) return null;
                    return new ElementOwnerTransformer(api, super.visitMethod(access, name, descriptor, signature, exceptions), Type.getInternalName(parent), certifiedClazzName);
                };
            };
            ClassReader reader = new ClassReader(Resources.toByteArray(Main.class.getClassLoader().getResource(Type.getInternalName(parent) + ".class")));
            reader.accept(c, ClassReader.EXPAND_FRAMES);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        PrintWriter pw = null;
		try { 
			pw = new PrintWriter(new File(currentDirFile, "ohshit.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        CheckClassAdapter cca = new CheckClassAdapter(cw);
        TraceClassVisitor tcv = new TraceClassVisitor(cca, pw);

        MyClassVisitor cv = new MyClassVisitor(tcv) {
            @Override
            public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
                if(name.equals("LOCK") || name.equals("loadFactor")) {
                    return super.visitField(access | Opcodes.ACC_FINAL, name, descriptor, signature, value);
                }
                return super.visitField(access, name, descriptor, signature, value);
            }
        };
        cv.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC, certifiedClazzName, null, Type.getInternalName(parent), new String[0]); // "biom4st3r.asm.MultiListAsm".replace('.', '/')
        cv.visitSource("Biom4st3r", "wtf?");
        for(int i = 0; i < clazzes.length; i++) {
            FieldVisitor fv = cv.visitField(Opcodes.ACC_PROTECTED, i + "array", Type.getDescriptor(arrayType(clazzes[i])), null, null);
            fv.visitEnd();
        }
        int currLine = 0;
        OpcodeMethodVisitor mv = cv.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "(I)V", null, null);
        { // CTOR
            mv.visitCode();
            Label l0 = new Label();
            mv.visitLabel(l0);
            mv.ALOAD(0);
            mv.ILOAD(1);
            mv.visitMethodInsn(Opcodes.INVOKESPECIAL, Type.getInternalName(parent), "<init>", "(I)V", false);
            mv.visitLineNumber(currLine++, l0);
            mv.visitLocalVariable("loadFactor", "I", null, l0, l0, 1);
            mv.ALOAD(0);
            mv.visitInsn(Opcodes.ICONST_0);
            mv.visitFieldInsn(Opcodes.PUTFIELD, certifiedClazzName, "index", "I");
            mv.visitLineNumber(currLine++, l0);
            mv.ALOAD(0);
            Asm.ctor(mv, (vistior)->{}, "java.util.concurrent.locks.ReentrantLock", "()V");
            mv.visitFieldInsn(Opcodes.PUTFIELD, certifiedClazzName, "LOCK", "Ljava/util/concurrent/locks/ReentrantLock;");
            mv.visitLineNumber(currLine++, l0);
            { // init Arrays
                mv.ALOAD(0);
                mv.ILOAD(1);
                mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, certifiedClazzName, "reinit", "(I)V", false);
                mv.visitLineNumber(currLine++, l0);
            }
            Label l1 = new Label();
            mv.visitLabel(l1);
            mv.RETURN();
            mv.visitLineNumber(currLine++, l1);
            mv.visitMaxs(3, 2); // CCA told me to do 3,2 instead of 2,2
            mv.visitEnd();
        }

        mv = cv.visitMethod(Opcodes.ACC_PUBLIC, "reinit", "(I)V", null, null);
        { // REINIT
            mv.visitCode();
            List<Label> ls = Lists.newArrayList();
            
            for(@SuppressWarnings({"unused"}) Object o : clazzes) {
                ls.add(new Label());
            }
            
            for(int i = 0; i < clazzes.length; i++) {
                Label lx = ls.get(i);
                mv.visitLabel(lx);
                mv.ALOAD(0);
                mv.ILOAD(1);
                if(Asm.isPrimitive(clazzes[i])) {
                    mv.visitIntInsn(Opcodes.NEWARRAY, Asm.get(clazzes[i]).arrayType());
                }
                else {
                    mv.visitTypeInsn(Opcodes.ANEWARRAY, Type.getInternalName(clazzes[i]));
                }
                mv.visitFieldInsn(Opcodes.PUTFIELD, certifiedClazzName, i+"array", Type.getDescriptor(arrayType(clazzes[i])));
                mv.visitLineNumber(currLine++, lx);
            }
            
            mv.RETURN();
            mv.visitLocalVariable("newSize", "I", null, ls.get(0), ls.get(ls.size()-1), 1);
            mv.visitMaxs(2, 2);
            mv.visitEnd();
        }
        mv = cv.visitMethod(Opcodes.ACC_PUBLIC, "getArraySize", "()I", null, null);
        { // getArraySize
            Label lx = new Label();
            mv.visitCode();
            mv.visitLabel(lx);
            mv.ALOAD(0);
            mv.GET_FIELD(certifiedClazzName, "0array", Type.getDescriptor(arrayType(clazzes[0])));
            mv.visitInsn(Opcodes.ARRAYLENGTH);
            mv.IRETURN();
            mv.visitLineNumber(currLine++, lx);
            mv.visitMaxs(1, 1);
            mv.visitEnd();
        }

        // GETTERS AND PUTTERS
        // Reduce Type to Primitive supporting clazzes and dedupe
        for(Class<?> clazz : Stream.of(clazzes).map((c)->Asm.get(c).clazz).distinct().collect(Collectors.toSet())) {
            Collection<Primitive> prim = Stream.of(clazzes).map((c)->Asm.get(c)).collect(Collectors.toSet());
            if(!prim.contains(Asm.get(clazz))) {
                continue;
            }
            String getterName = "_get" + clazz.getName().substring(0,1).toUpperCase() + clazz.getName().substring(1);
            if(Asm.get(clazz) == Primitive.OBJECT) {
                getterName = "_getObject";
            }
            String putterName = "_put" + clazz.getName().substring(0,1).toUpperCase() + clazz.getName().substring(1);
            if(Asm.get(clazz) == Primitive.OBJECT) {
                putterName = "_putObject";
            }
            mv = cv.visitMethod(Opcodes.ACC_PUBLIC, getterName, "(II)"+Type.getDescriptor(Asm.get(clazz).clazz), null, null);
            { // _getx
                mv.visitCode();
                Label lx = new Label();
                mv.visitLabel(lx);
                mv.visitLocalVariable("array", "I", null, lx, lx, 1);
                mv.visitLocalVariable("index", "I", null, lx, lx, 2);

                Label dflt = new Label();
                List<Label> label = Lists.newArrayList();
                for(int i = 0; i < clazzes.length; i++) {
                    if(Asm.get(clazzes[i]) == Asm.get(clazz)) {
                        label.add(new Label());
                    }
                    else {
                        label.add(dflt);
                    }
                }

                mv.ILOAD(1);
                mv.visitTableSwitchInsn(0, label.size()-1, dflt, label.toArray(new Label[0]));
                mv.visitLineNumber(currLine++, lx);
 
                for(int i = 0; i < clazzes.length; i++) {
                    Label l = label.remove(0);
                    if(Asm.get(clazzes[i]) == Asm.get(clazz)) {
                        mv.visitLabel(l);
                        mv.ALOAD(0);
                        mv.GET_FIELD(certifiedClazzName, i+"array", Type.getDescriptor(arrayType(clazzes[i])));
                        mv.ILOAD(2);
                        Asm.get(clazzes[i]).touchArray(Stack.LOAD, mv);
                        Asm.get(clazzes[i])._return(mv);
                        mv.visitLineNumber(currLine++, l);
                    }
                }
                mv.visitLabel(dflt);
                Asm.get(clazz).loadDefaultValue(mv);
                Asm.get(clazz)._return(mv);
                mv.visitLineNumber(currLine++, dflt);
                mv.visitMaxs(2, 3); // 0,3 -> 2,3
                mv.visitEnd();
            }
            mv = cv.visitMethod(Opcodes.ACC_PUBLIC, putterName, String.format("(II%s)V", Type.getDescriptor(clazz)), null, null);
            { // _putX
                mv.visitCode();
                Label l0 = new Label();
                mv.visitLabel(l0);
                mv.visitLocalVariable("array", "I", null, l0, l0, 1);
                mv.visitLocalVariable("index", "I", null, l0, l0, 2);
                mv.visitLocalVariable("val", Type.getDescriptor(clazz), null, l0, l0, 3);

                mv.ALOAD(0);
                mv.ILOAD(2);
                mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, certifiedClazzName, "checkSize", "(I)V", false);
                mv.visitLineNumber(currLine++, l0);
                Label dflt = new Label();
                List<Label> label = Lists.newArrayList();
                for(int i = 0; i < clazzes.length; i++) {
                    if(Asm.get(clazzes[i]) == Asm.get(clazz)) {
                        label.add(new Label());
                    }
                    else {
                        label.add(dflt);
                    }
                }

                mv.ILOAD(1);
                mv.visitTableSwitchInsn(0, label.size()-1, dflt, label.toArray(new Label[0]));
                mv.visitLineNumber(currLine++, l0);

                for(int i = 0; i < clazzes.length; i++) {
                    Label l = label.remove(0);
                    if(Asm.get(clazzes[i]) == Asm.get(clazz)) {
                        mv.visitLabel(l);
                        mv.ALOAD(0);
                        mv.GET_FIELD(certifiedClazzName, i+"array", Type.getDescriptor(arrayType(clazzes[i])));
                        mv.ILOAD(2);
                        Asm.get(clazzes[i]).touchVar(Stack.LOAD, mv, 3);
                        Asm.get(clazzes[i]).touchArray(Stack.STORE, mv);
                        mv.RETURN();
                        mv.visitLineNumber(currLine++, l);
                    }
                }

                mv.visitLabel(dflt);
                mv.RETURN();
                mv.visitLineNumber(currLine++, dflt);

                mv.visitMaxs(3, 5);
                mv.visitEnd();
            }
        }
        mv = cv.visitMethod(Opcodes.ACC_PROTECTED, "resize", "()V", null, null);
        {
            mv.visitCode();
            Label l0 = new Label();
            mv.visitLabel(l0);

            List<Label> labels = Lists.newArrayListWithCapacity(clazzes.length);
            for(int i = 0; i < clazzes.length; i++) {
                labels.add(new Label());
            }

            mv.ALOAD(0);
            mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, certifiedClazzName, "getArraySize", "()I", false);
            mv.ALOAD(0);
            mv.GET_FIELD(certifiedClazzName, "index", "I");
            mv.IADD();
            mv.ISTORE(2);
            mv.visitLineNumber(currLine++, l0);

            for (int i = 0; i < clazzes.length; i++) {
                mv.visitLabel(labels.get(i));
                mv.ALOAD(0);
                mv.DUP();
                mv.GET_FIELD(certifiedClazzName, i+"array", Type.getDescriptor(arrayType(clazzes[i])));
                mv.ILOAD(2);
                String desc = Type.getDescriptor(arrayType(Asm.get(clazzes[i]).clazz));
                mv.visitMethodInsn(Opcodes.INVOKESTATIC, Type.getInternalName(Arrays.class), "copyOf", String.format("(%sI)%s", desc, desc), false);
                if(Asm.get(clazzes[i]) == Primitive.OBJECT) {
                    mv.CHECKCAST(Type.getInternalName(arrayType(clazzes[i])));
                }
                mv.visitFieldInsn(Opcodes.PUTFIELD, certifiedClazzName, i+"array", Type.getDescriptor(arrayType(clazzes[i])));
                mv.visitLineNumber(currLine++, labels.get(i));
            }
            Label end = new Label();
            mv.visitLabel(end);
            mv.RETURN();
            mv.visitLineNumber(currLine++, end);
            mv.visitLocalVariable("size", "I", null, l0, labels.get(labels.size()-1), 2);
            mv.visitMaxs(3, 3); // 3,3
            mv.visitEnd();
        }
        mv = cv.visitMethod(Opcodes.ACC_PUBLIC, "remove", "(I)Z", null, null);
        {
            mv.visitCode();
            Label l0 = new Label();
            Label theLoop = new Label();
            mv.visitLabel(l0);
            mv.ALOAD(0);
            mv.ILOAD(1);
            mv.visitMethodInsn(Opcodes.INVOKESPECIAL, Type.getInternalName(parent), "remove", "(I)Z", false);
            mv.visitJumpInsn(Opcodes.IFEQ, theLoop);
            mv.visitLineNumber(currLine++, l0);

            Asm.pushInt(mv, 1);
            mv.IRETURN();
            mv.visitLineNumber(currLine++, l0);

            mv.visitLabel(theLoop);
            mv.ALOAD(0);
            mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, certifiedClazzName, "lock", "()V", false);
            mv.visitLineNumber(currLine++, theLoop);
            for(int i = 0; i < clazzes.length; i++) {
                mv.visitLabel(new Label());
                mv.ALOAD(0);
                    mv.GET_FIELD(certifiedClazzName, i+"array", arrayType(clazzes[i]));
                    mv.ILOAD(1);
                        Asm.pushInt(mv, 1);
                            mv.IADD();
                        mv.ALOAD(0);
                            mv.GET_FIELD(certifiedClazzName, i+"array", arrayType(clazzes[i]));
                            mv.ILOAD(1);
                                mv.ALOAD(0);
                                    mv.GET_FIELD(certifiedClazzName, "index", "I");
                                mv.ILOAD(1);
                                    Asm.pushInt(mv, 1);
                                        mv.IADD();
                                    mv.ISUB();
                mv.visitMethodInsn(Opcodes.INVOKESTATIC, Type.getInternalName(System.class), "arraycopy", "(Ljava/lang/Object;ILjava/lang/Object;II)V", false);
                mv.visitLineNumber(currLine++, theLoop);
            }
            mv.ALOAD(0);
            mv.DUP();
            mv.DUP();
            mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, certifiedClazzName, "unlock", "()V", false);
            mv.visitLineNumber(currLine++, theLoop);
            mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, certifiedClazzName, "revokeLatest", "()V", false);
            mv.visitLineNumber(currLine++, theLoop);
            mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, certifiedClazzName, "trim", "()V", false);
            mv.visitLineNumber(currLine++, theLoop);
            mv.visitLabel(new Label());
            Asm.pushInt(mv, 1);
            mv.IRETURN();
            mv.visitLineNumber(currLine++, theLoop);
            mv.visitMaxs(7, 3);
            mv.visitEnd();
        }
        mv = cv.visitMethod(Opcodes.ACC_PUBLIC, "getCopy", "()" + Type.getDescriptor(MultiListAsm.class), null, null); 
        {
            mv.visitCode();
            Label l0 = new Label();
            Label end = new Label();
            mv.visitLabel(l0);
            Asm.ctor(mv, (v)->{v.visitVarInsn(Opcodes.ALOAD, 0);v.visitFieldInsn(Opcodes.GETFIELD, certifiedClazzName, "loadFactor", "I");}, certifiedClazzName, "(I)V");
            mv.DUP();
            mv.ASTORE(1);
            mv.ALOAD(0);
            mv.GET_FIELD(certifiedClazzName, "index", "I");
            mv.PUT_FIELD(certifiedClazzName, "index", "I");
            for(int i = 0; i < clazzes.length; i++ ) {
                mv.ALOAD(1);
                mv.ALOAD(0);
                mv.GET_FIELD(certifiedClazzName, i+"array", arrayType(clazzes[i]));
                mv.DUP();
                mv.ARRAY_LENGTH();                
                String desc = Type.getDescriptor(arrayType(Asm.get(clazzes[i]).clazz));
                mv.visitMethodInsn(Opcodes.INVOKESTATIC, Type.getInternalName(Arrays.class), "copyOf", String.format("(%sI)%s", desc, desc), false);
                if(Asm.get(clazzes[i]) == Primitive.OBJECT) {
                    mv.CHECKCAST(Type.getInternalName(arrayType(clazzes[i])));
                }
                mv.PUT_FIELD(certifiedClazzName, i+"array", arrayType(clazzes[i]));
            }
            mv.visitLabel(end);
            mv.ALOAD(1);
            mv.ARETURN();
            mv.visitLocalVariable("newobj", "L"+certifiedClazzName+";", null, l0, end, 1);
            mv.visitMaxs(3, 2);
            mv.visitEnd();
        }

        mv = cv.visitMethod(Opcodes.ACC_PUBLIC, "test0", "()V", null, null);
        { // getArraySize
            Label lx = new Label();
            mv.visitCode();
            mv.visitLabel(lx);
            mv.ALOAD(0);
            mv.DUP();
            mv.RETURN();
            mv.visitLineNumber(currLine++, lx);
            mv.visitMaxs(2, 1);
            mv.visitEnd();
        }

        mv = cv.visitMethod(Opcodes.ACC_PUBLIC, "test1", "()V", null, null);
        { // getArraySize
            Label lx = new Label();
            mv.visitCode();
            mv.visitLabel(lx);
            mv.ALOAD(0);
            mv.ALOAD(0);
            mv.RETURN();
            mv.visitLineNumber(currLine++, lx);
            mv.visitMaxs(2, 1);
            mv.visitEnd();
        }

        cv.visitEnd();
        try {
            FileOutputStream fos = new FileOutputStream(new File(currentDirFile, "ohshit.class"));
            fos.write(cw.toByteArray());
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        map.put(clazzName, defineClass.invoke(Thread.currentThread().getContextClassLoader(), cw.toByteArray(), 0, cw.toByteArray().length));
        try {
            MultiListAsm asm = CtorRef.<MultiListAsm>getCtor(map.get(clazzName), 0, int.class).newInstance(loadFactor);
            return (T) asm;
        }
        catch(Throwable t) {
            throw new RuntimeException(t);
        }
    }

    static boolean contains(Class<?>[] clazzes , Class<?> clazz) {
        for(Class<?> c : clazzes ) {
            if(c == clazz) return true;
        }
        return false;
    }

    public MultiListAsm(int loadFactor) {
        this.loadFactor = loadFactor;
    }

    protected ReentrantLock LOCK;
    protected int index;
    protected final int loadFactor;

    public final void lock() {
        LOCK.lock();
    }

    public final void unlock() {
        LOCK.unlock();
    }

    public final void checkSize(int index) {
        if(index == this.getArraySize()) {
            this.resize();
        }
    }

    public final void clear() {
        this.reinit(this.loadFactor);
    }

    public final void revokeLatest() {
        this.index--;
    }

    public final int getLength() {
        return index;
    }

    public final boolean isEmpty() {
        return this.index == 0;
    }

    public final void trim() {
        if(this.getArraySize() > this.getLength() + loadFactor)
        {
            this.resize();
        }
    }

    @AsmControlled protected int getArraySize() {return -1;} // DONE
    @AsmControlled public void reinit(int newSize) {} // DONE
    @AsmControlled protected void resize() {} // DONE
    @AsmControlled public boolean remove(int index) { // DONE
        if(index == this.getLength() -1) {
            this.revokeLatest();
            return true;
        }
        return false;
    }

    @AsmControlled public void test0() {}
    @AsmControlled public void test1() {}

    @AsmControlled protected int _getInt(int array, int index) {return -1;} // DONE
    @AsmControlled protected <T> T _getObject(int array, int index) {return null;} // DONE
    @AsmControlled protected byte _getByte(int array, int index) {return -1;} // DONE
    @AsmControlled protected short _getShort(int array, int index) {return -1;} // DONE
    @AsmControlled protected long _getLong(int array, int index) {return -1;} // DONE
    @AsmControlled protected float _getFloat(int array, int index) {return -1;} // DONE
    @AsmControlled protected double _getDouble(int array, int index) {return -1;} // DONE
    @AsmControlled protected boolean _getBoolean(int array, int index) {return false;} // DONE
    @AsmControlled protected char _getChar(int array, int index) {return '0';} // DONE

    @AsmControlled protected void _putInt(int array, int index, int val) {} // DONE
    @AsmControlled protected <T> void _putObject(int array, int index, T val) {} // DONE
    @AsmControlled protected void _putByte(int array, int index, byte val) {} // DONE
    @AsmControlled protected void _putShort(int array, int index, short val) {} // DONE
    @AsmControlled protected void _putLong(int array, int index, long val) {} // DONE
    @AsmControlled protected void _putFloat(int array, int index, float val) {} // DONE
    @AsmControlled protected void _putDouble(int array, int index, double val) {} // DONE
    @AsmControlled protected void _putBoolean(int array, int index, boolean val) {} // DONE
    @AsmControlled protected void _putChar(int array, int index, char val) {} // DONE
    @AsmControlled public <T extends MultiListAsm> T getCopy() {return null;}
}
